package ui;

import config.GameConfig;
import dto.GameDto;

import javax.swing.*;
import java.awt.*;

/**
 * Created by GanJc on 2015/7/5.
 */
public abstract class Layer {

    /**
     * 边框大小
     */
    protected static final int SIZE;

    /**
     * 边距
     */
    protected static final int PADDING;

    /**
     * 边框图片
     */
    private static final Image WINDOW_IMG = new ImageIcon(Layer.class.getResource("/graphics/window/Window.png")).getImage();

    /**
     * 边框图片宽度
     */
    private static final int WINDOW_IMG_WIDTH = WINDOW_IMG.getWidth(null);

    /**
     * 边框图片高度
     */
    private static final int WINDOW_IMG_HEIGHT = WINDOW_IMG.getHeight(null);

    /**
     * 数字图片
     */
    protected static final Image NUMBER_IMG = new ImageIcon(Layer.class.getResource("/graphics/string/num.png")).getImage();

    /**
     * 单个数字宽度
     */
    protected static final int NUMBER_IMG_W = NUMBER_IMG.getWidth(null) / 10;

    /**
     * 单个数字高度
     */
    protected static final int NUMBER_IMG_H = NUMBER_IMG.getHeight(null);

    static {
        GameConfig gameConfig = GameConfig.getInstance();
        SIZE = gameConfig.getSize();
        PADDING = gameConfig.getPadding();
    }

    protected int x;
    protected int y;
    protected int w;
    protected int h;

    protected GameDto dto;

    public Layer(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    /**
     * 绘制窗口
     *
     * @param g 画笔
     */
    public void createWindow(Graphics g) {
        //左上角
        g.drawImage(WINDOW_IMG, x, y, x + SIZE, y + SIZE,     //表现在panel的哪里
                0, 0, SIZE, SIZE, null);                      //从图片的哪里开始截图
        //上边
        g.drawImage(WINDOW_IMG, x + SIZE, y, x + w - SIZE, y + SIZE,
                SIZE, 0, WINDOW_IMG_WIDTH - SIZE, SIZE, null);
        //右上角
        g.drawImage(WINDOW_IMG, x + w - SIZE, y, x + w, y + SIZE,
                WINDOW_IMG_WIDTH - SIZE, 0, WINDOW_IMG_WIDTH, SIZE, null);
        //左边
        g.drawImage(WINDOW_IMG, x, y + SIZE, x + SIZE, y + h - SIZE,
                0, SIZE, SIZE, WINDOW_IMG_HEIGHT - SIZE, null);
        //正中
        g.drawImage(WINDOW_IMG, x + SIZE, y + SIZE, x + w - SIZE, y + h - SIZE,
                SIZE, SIZE, WINDOW_IMG_WIDTH - SIZE, WINDOW_IMG_HEIGHT - SIZE, null);
        //右边
        g.drawImage(WINDOW_IMG, x + w - SIZE, y + SIZE, x + w, y + h - SIZE,
                WINDOW_IMG_WIDTH - SIZE, SIZE, WINDOW_IMG_WIDTH, WINDOW_IMG_HEIGHT - SIZE, null);
        //左下角
        g.drawImage(WINDOW_IMG, x, y + h - SIZE, x + SIZE, y + h,
                0, WINDOW_IMG_HEIGHT - SIZE, SIZE, WINDOW_IMG_HEIGHT, null);
        //下边
        g.drawImage(WINDOW_IMG, x + SIZE, y + h - SIZE, x + w - SIZE, y + h,
                SIZE, WINDOW_IMG_HEIGHT - SIZE, WINDOW_IMG_WIDTH - SIZE, WINDOW_IMG_HEIGHT, null);
        //右下角
        g.drawImage(WINDOW_IMG, x + w - SIZE, y + h - SIZE, x + w, y + h,
                WINDOW_IMG_WIDTH - SIZE, WINDOW_IMG_HEIGHT - SIZE, WINDOW_IMG_WIDTH, WINDOW_IMG_HEIGHT, null);
    }

    public void setDto(GameDto dto) {
        this.dto = dto;
    }

    public void paint(Graphics g) {
        createWindow(g);
    }

    /**
     * 将图片绘制与窗体中间
     *
     * @param g     画笔
     * @param image 图片
     */
    protected void drawImageAtCenter(Graphics g, Image image) {
        int height = image.getHeight(null);
        int width = image.getWidth(null);
        g.drawImage(image, x + (w - width >> 1), y + (h - height >> 1), null);
    }

    /**
     * 绘制数字(左填充)
     *
     * @param g        画笔
     * @param x1       指定位置x坐标
     * @param y1       指定位置y坐标
     * @param maxCount 最大位数
     * @param num      数字
     */
    protected void drawNumberStringLeftPad(Graphics g, int x1, int y1, int maxCount, int num) {
        char[] chars = Integer.toString(num).toCharArray();
        for (int i = 0; i < maxCount; i++) {
            if (maxCount - i <= chars.length) {
                int idx = i - maxCount + chars.length;
                char aChar = chars[idx];
                Integer integer = Integer.valueOf(Character.toString(aChar));
                g.drawImage(NUMBER_IMG,
                        x + x1 + i * NUMBER_IMG_W,
                        y + y1,
                        x + x1 + (i + 1) * NUMBER_IMG_W,
                        y + y1 + NUMBER_IMG_H,
                        integer * NUMBER_IMG_W, 0, (integer + 1) * NUMBER_IMG_W, NUMBER_IMG_H, null);
            }
        }
    }

}
