package ui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by GanJc on 2015/7/6.
 */
public class LayerLevel extends  Layer {

    /**
     * 标题图片
     */
    private static final Image LEVEL_STRING_IMG = new ImageIcon(Layer.class.getResource("/graphics/string/level.png")).getImage();

    /**
     * LEVEL_STRING_IMG 图片宽度
     */
    private static final int LEVEL_STRING_IMG_W = LEVEL_STRING_IMG.getWidth(null);


    public LayerLevel(int x, int y, int w, int h) {
        super(x, y, w, h);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        int centerX = w - LEVEL_STRING_IMG_W >> 1;
        g.drawImage(LEVEL_STRING_IMG, x + centerX, y + PADDING, null);
        drawNumberStringLeftPad(g, centerX, 64, 2, dto.getNowLevel());
    }

}
