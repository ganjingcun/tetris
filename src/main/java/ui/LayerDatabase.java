package ui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by je on 2015/7/6.
 */
public class LayerDatabase extends Layer {

    private static Image DB_IMG = new ImageIcon(Layer.class.getResource("/graphics/string/db.png")).getImage();

    public LayerDatabase(int x, int y, int w, int h) {
        super(x,y,w,h);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(DB_IMG,x+PADDING,y+PADDING,null);
    }
}
