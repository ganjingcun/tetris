package ui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by GanJc on 2015/7/6.
 */
public class LayerPoint extends Layer {

    /**
     * 标题图片(分数)
     */
    private static final Image POINT_STRING_IMG = new ImageIcon(Layer.class.getResource("/graphics/string/point.png")).getImage();

    /**
     *
     */
    private static final Image RECT_IMG = new ImageIcon(Layer.class.getResource("/graphics/window/rect.png")).getImage();

    /**
     *
     */
    private static final int RECT_IMG_H = RECT_IMG.getHeight(null);
    private static final int RECT_IMG_W = RECT_IMG.getWidth(null);



    /**
     * 标题图片(分数) y轴位置
     */
    private static final int POINT_Y = PADDING;

    /**
     * 标题图片(消行)
     */
    private static final Image REMOVE_LINE_IMG = new ImageIcon(Layer.class.getResource("/graphics/string/rmline.png")).getImage();

    /**
     * 标题图片(消行) y轴位置
     */
    private static final int REMOVE_Y = POINT_STRING_IMG.getHeight(null) + (PADDING << 1);

    /**
     * 分数最大位数
     */
    private static final int MAX_BIT = 5;

    private static int POINT_X ;

    private final int expY ;

    private final int expW ;

    private final int levelUp = 20 ;





    public LayerPoint(int x, int y, int w, int h) {
        super(x,y,w,h);
        POINT_X = this.w - NUMBER_IMG_W * MAX_BIT -PADDING ;
        expW = w - (PADDING<<1);
        expY = (REMOVE_LINE_IMG.getHeight(null)<<1) +PADDING * 3 ;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        int rectX = x + PADDING;
        int rectY = y + expY;

        g.drawImage(POINT_STRING_IMG, rectX, y + POINT_Y, null);
        drawNumberStringLeftPad(g, POINT_X, POINT_Y, MAX_BIT, dto.getNowPoint());
        g.drawImage(REMOVE_LINE_IMG, rectX, y + REMOVE_Y, null);
        drawNumberStringLeftPad(g, POINT_X, REMOVE_Y, MAX_BIT, dto.getNowRemoveLine());

        g.setColor(Color.BLACK);

        g.fillRect(rectX, rectY, expW, RECT_IMG_H+4);
        g.setColor(Color.WHITE);
        g.fillRect(rectX + 1, rectY + 1, expW - 2, RECT_IMG_H+2);
        g.setColor(Color.BLACK);
        g.fillRect(rectX + 2, rectY + 2, expW - 4, RECT_IMG_H);

        int w = (int) ((dto.getNowRemoveLine() % (double)levelUp) / levelUp * (expW - 4));

//        drawRect(g, w);

        g.drawImage(RECT_IMG,
                rectX + 2, rectY + 2,
                rectX + 2 + w, rectY + RECT_IMG_H + 2,
                0, 0, 255, RECT_IMG_H,
                null);


    }

    private void drawRect(Graphics g, int w) {
        g.setColor(getColor((dto.getNowRemoveLine() % (double) levelUp), levelUp));
        g.fillRect(x + PADDING + 2, y + expY + 2, w, RECT_IMG_H);
    }

    private Color getColor(double hp ,double maxHp){
        int colorR = 0 ;
        int colorG = 255 ;
        int colorB = 0 ;
        double halfHp  = maxHp/2;
        if(hp>halfHp){
            colorR= 255 - (int)((hp-halfHp)/halfHp*255);
            colorG= 255;
        }else {
            colorR= 255;
            colorG= (int)(hp/(halfHp)*255);
        }

        return new Color(colorR,colorG,colorB);

    }

}
