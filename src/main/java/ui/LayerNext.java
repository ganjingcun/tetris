package ui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by GanJc on 2015/7/6.
 */
public class LayerNext extends Layer {

    private static Image[] IMAGES;

    static {
        IMAGES = new Image[7];
        for (int i = 0; i < IMAGES.length; i++) {
            IMAGES[i] = new ImageIcon(Layer.class.getResource("/graphics/game/" + i + ".png")).getImage();
        }
    }

    public LayerNext(int x, int y, int w, int h) {
        super(x, y, w, h);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        drawImageAtCenter(g,IMAGES[dto.getNext()]);
    }

}
