package ui;

import entity.GameCell;

import javax.swing.*;
import java.awt.*;

/**
 * Created by GanJc on 2015/7/6.
 */
public class LayerGame extends Layer {

    private static Image ACT_IMG = new ImageIcon(Layer.class.getResource("/graphics/game/rect.png")).getImage();

    public LayerGame(int x, int y, int w, int h) {
        super(x, y, w, h);
    }

    /**
     * 左位移偏移量
     */
    private int SIZE_ROL = 5;

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        GameCell cell = dto.getCell();
        int typeCode = cell.getTypeCode();
        Point[] actPoint = cell.getPoints();
        //画方块
        for (Point point : actPoint) {
            drawCell(g, point.x, point.y, typeCode + 1);
        }
        //画地图
        boolean[][] gameMap = dto.getGameMap();
        for (int x1 = 0; x1 < gameMap.length; x1++) {
            for (int y1 = 0; y1 < gameMap[x1].length; y1++) {
                if (gameMap[x1][y1]) {
                    drawCell(g, x1, y1, 0);
                }
            }
        }

    }

    /**
     * 绘制方格
     * @param g 画笔
     * @param x1 方格的x坐标
     * @param y1 方格的y坐标
     * @param imgIndex 用于切图
     */
    private void drawCell(Graphics g, int x1, int y1, final int imgIndex) {
        g.drawImage(ACT_IMG,
                x + (x1 << SIZE_ROL) + 7,
                y + (y1 << SIZE_ROL) + 7,
                x + (x1 + 1 << SIZE_ROL) + 7,
                y + (y1 + 1 << SIZE_ROL) + 7,
                imgIndex << SIZE_ROL, 0, imgIndex + 1 << SIZE_ROL, 1 << SIZE_ROL, null
        );
    }


}
