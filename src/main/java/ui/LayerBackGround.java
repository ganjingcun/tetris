package ui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by je on 2015/7/6.
 */
public class LayerBackGround extends Layer {

    private static Image BACK_GROUND_IMG = new ImageIcon(Layer.class.getResource("/graphics/background/Sea.jpg")).getImage();


    public LayerBackGround(int x, int y, int w, int h) {
        super(x, y, w, h);
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(BACK_GROUND_IMG,0,0,1162,654,null);
    }
}
