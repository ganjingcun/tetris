package ui;

import config.GameConfig;

import javax.swing.*;

/**
 * Created by GanJc on 15-7-2 下午9:51
 */
public class JFrameGame extends JFrame {

    public JFrameGame(JPanelGame jPanelGame) {
        GameConfig gameConfig = GameConfig.getInstance();
        this.setTitle(gameConfig.getTitle());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(gameConfig.getWidth(),gameConfig.getHeight());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setContentPane(jPanelGame);
        this.setVisible(true);
    }

}
