package ui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by je on 2015/7/6.
 */
public class LayerDisk extends Layer {

    private static Image DISK_IMG = new ImageIcon(Layer.class.getResource("/graphics/string/disk.png")).getImage();

    public LayerDisk(int x, int y, int w, int h) {
        super(x,y,w,h);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(DISK_IMG,x+PADDING,y+PADDING,null);
    }
}
