package main;


import controller.GameController;
import controller.PlayerController;
import dto.GameDto;
import service.GameService;
import ui.JFrameGame;
import ui.JPanelGame;

/**
 * Created by GanJc on 15-7-2 下午9:51
 */
public class Main {

    public static void main(String[] args) {
        GameDto dto = new GameDto();
        GameService gameService = new GameService(dto);
        JPanelGame jPanelGame = new JPanelGame(dto);
        GameController gameController = new GameController(jPanelGame,gameService);
        PlayerController playerController = new PlayerController(gameController);
        jPanelGame.setGameController(playerController);
        JFrameGame jFrameGame = new JFrameGame(jPanelGame);
    }




}
