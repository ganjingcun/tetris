package dto;

import entity.GameCell;

import java.util.List;

/**
 * Created by GanJc on 2015/7/7.
 */
public class GameDto {

    public GameDto() {
        this.gameMap = new boolean[10][18];
    }

    private List<Player> dbRecord ;

    private List<Player> localRecord ;

    private boolean [][] gameMap ;

    private GameCell cell ;

    private int next ;

    private int nowLevel ;

    private int nowPoint ;

    private int nowRemoveLine ;

    public List<Player> getDbRecord() {
        return dbRecord;
    }

    public void setDbRecord(List<Player> dbRecord) {
        this.dbRecord = dbRecord;
    }

    public List<Player> getLocalRecord() {
        return localRecord;
    }

    public void setLocalRecord(List<Player> localRecord) {
        this.localRecord = localRecord;
    }

    public boolean[][] getGameMap() {
        return gameMap;
    }

    public void setGameMap(boolean[][] gameMap) {
        this.gameMap = gameMap;
    }

    public GameCell getCell() {
        return cell;
    }

    public void setCell(GameCell cell) {
        this.cell = cell;
    }

    public int getNext() {
        return next;
    }

    public void setNext(int next) {
        this.next = next;
    }

    public int getNowLevel() {
        return nowLevel;
    }

    public void setNowLevel(int nowLevel) {
        this.nowLevel = nowLevel;
    }

    public int getNowPoint() {
        return nowPoint;
    }

    public void setNowPoint(int nowPoint) {
        this.nowPoint = nowPoint;
    }

    public int getNowRemoveLine() {
        return nowRemoveLine;
    }

    public void setNowRemoveLine(int nowRemoveLine) {
        this.nowRemoveLine = nowRemoveLine;
    }
}
