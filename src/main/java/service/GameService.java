package service;

import dto.GameDto;
import entity.GameCell;

import java.awt.*;
import java.util.Random;

/**
 * Created by GanJc on 2015/7/7.
 */
public class GameService {

    private GameDto dto;

    private Random random = new Random();

    private static final int MAX_TYPE = 6;

    public GameService(GameDto dto) {
        this.dto = dto;
        GameCell cell = new GameCell(random.nextInt(MAX_TYPE));
        this.dto.setCell(cell);
    }

    public void keyUp() {
        dto.setNowLevel(dto.getNowLevel() + 1);
        dto.setNowPoint(dto.getNowPoint() + 2);
        dto.setNowRemoveLine(dto.getNowRemoveLine() + 1);
        dto.getCell().rotate(dto.getGameMap());
    }

    public void keyDown() {
        if (dto.getCell().move(0, 1, dto.getGameMap())) {
            return;
        }

        boolean[][] gameMap = dto.getGameMap();
        Point[] points = dto.getCell().getPoints();
        for (Point point : points) {
            gameMap[point.x][point.y] = true;
        }
        //TODO 消行
        //计分
        //升级

        //创建新的方块
        dto.getCell().init(dto.getNext());
        //生成方块下一个
        dto.setNext(random.nextInt(MAX_TYPE));
    }

    public void keyLeft() {
        dto.getCell().move(-1, 0, dto.getGameMap());
    }

    public void keyRight() {
        dto.getCell().move(1, 0, dto.getGameMap());
    }

}
