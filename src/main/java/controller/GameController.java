package controller;

import service.GameService;
import ui.JPanelGame;

/**
 * 接受玩家键盘事件
 * 控制画面
 * 控制业务逻辑
 * Created by GanJc on 2015/7/7.
 */
public class GameController {

    /**
     * 游戏界面层
     */
    private JPanelGame panelGame ;

    /**
     * 游戏逻辑层
     */
    private GameService gameService ;

    public GameController(JPanelGame panelGame, GameService gameService) {
        this.panelGame = panelGame;
        this.gameService = gameService;
    }

    public void test() {
        panelGame.repaint();
    }


    public void keyUp() {
        gameService.keyUp();
        panelGame.repaint();
    }

    public void keyDown() {
        gameService.keyDown();
        panelGame.repaint();
    }

    public void keyLeft() {
        gameService.keyLeft();
        panelGame.repaint();
    }

    public void keyRight() {
        gameService.keyRight();
        panelGame.repaint();
    }
}
