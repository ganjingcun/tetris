package controller;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by je on 2015/7/7.
 */
public class PlayerController extends KeyAdapter {

    private GameController gameController ;

    public PlayerController(GameController gameController) {
        this.gameController = gameController ;
    }

    /**
     * 键盘按下事件
     * @param e
     */
    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()){
            case KeyEvent.VK_UP:
                gameController.keyUp();
                break;
            case KeyEvent.VK_DOWN:
                gameController.keyDown();
                break;
            case KeyEvent.VK_LEFT:
                gameController.keyLeft();
                break;
            case KeyEvent.VK_RIGHT:
                gameController.keyRight();
                break;
            default:
                gameController.test();
        }
    }
}
