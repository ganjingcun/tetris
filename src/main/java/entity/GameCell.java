package entity;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by GanJc on 2015/7/7.
 */
public class GameCell {

    /**
     * 方块数组
     */
    private Point[] points;

    private static final int MIN_X = 0 ;
    private static final int MAX_X = 9 ;
    private static final int MIN_Y = 0 ;
    private static final int MAX_Y = 17 ;

    private int typeCode ;
    public GameCell(int typeCode) {
        this.typeCode = typeCode ;
        init(typeCode);
    }

    private static List<Point[]> TYPE_CONFIG ;

    static {
        TYPE_CONFIG = new ArrayList<Point[]>(7);
        TYPE_CONFIG.add(new Point[]{new Point(4, 0), new Point(3, 0), new Point(5, 0), new Point(6, 0)});
        TYPE_CONFIG.add(new Point[]{new Point(4, 0), new Point(3, 0), new Point(5, 0), new Point(4, 1)});
        TYPE_CONFIG.add(new Point[]{new Point(4, 0), new Point(3, 0), new Point(5, 0), new Point(3, 1)});
        TYPE_CONFIG.add(new Point[]{new Point(4, 0), new Point(5, 0), new Point(3, 1), new Point(4, 1)});
        TYPE_CONFIG.add(new Point[]{new Point(4, 0), new Point(5, 0), new Point(4, 1), new Point(5, 1)});
        TYPE_CONFIG.add(new Point[]{new Point(4, 0), new Point(3, 0), new Point(5, 0), new Point(5, 1)});
        TYPE_CONFIG.add(new Point[]{new Point(4, 0), new Point(3, 0), new Point(4, 1), new Point(5, 1)});
    }

    public void init(int typeCode) {
        this.typeCode = typeCode ;
        Point[] randomPoint = TYPE_CONFIG.get(typeCode);
        points = new Point[randomPoint.length];
        for (int i = 0; i < points.length; i++) {
            points[i] = new Point(randomPoint[i]);
        }
    }

    public Point[] getPoints() {
        return points;
    }

    /**
     * 方块移动
     * @param moveX x轴偏移量
     * @param moveY y轴偏移量
     * @param gameMap
     */
    public boolean move(int moveX, int moveY, boolean[][] gameMap){
        for (Point point : points) {
            int newX = point.x + moveX;
            int newY = point.y + moveY;
            if (isOutBound(newX,newY,gameMap)){
                return false;
            }
        }
        for (Point point : points) {
           point.translate(moveX,moveY);
        }
        return true ;
    }

    /**
     * 方块旋转
     * 笛卡尔旋转公式--顺时针:
     * A.X = 0.Y + O.X - B.Y
     * A.Y = O.Y - O.X + B.X
     * @param gameMap
     */
    public void rotate(boolean[][] gameMap){
        if (typeCode == 4){
            return;
        }
        for (Point point : points) {
            int newX = points[0].y + points[0].x - point.y;
            int newY = points[0].y - points[0].x + point.x;
            if (isOutBound(newX,newY, gameMap)) {
                return;
            }
        }
        for (Point point : points) {
            int newX = points[0].y + points[0].x - point.y;
            int newY = points[0].y - points[0].x + point.x;
            point.setLocation(newX, newY);
        }
    }

    private boolean isOutBound(int newX, int newY, boolean[][] gameMap){
        return newX < MIN_X || newX > MAX_X || newY < MIN_Y || newY > MAX_Y ||gameMap[newX][newY] ;
    }

    public int getTypeCode() {
        return typeCode;
    }
}
